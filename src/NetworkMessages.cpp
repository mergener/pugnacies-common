#include "../include/NetworkMessages.h"

#include <string.h>

//
//	Network messages format:
//
//		[2 bytes]	-->		size of data section
//		[2 bytes]	-->		message year
//		[1 byte]	-->		message month
//		[1 byte]	-->		message day
//		[1 byte]	-->		message hour
//		[1 byte]	-->		message minute
//		[2 bytes]	-->		message miliseconds
//		[2 bytes]	-->		message type
//		[n bytes]	-->		data section
//
//	OBS: Numbers are represented in LITTLE ENDIAN.
//

void TranslateNetworkMessage(const char* const inMsg, NetworkMessage* outMsg)
{
	outMsg->dataSize = ((uint16_t)inMsg[0] << 0) | ((uint16_t)inMsg[1] << 8);
	outMsg->time = {
		(uint16_t)((uint16_t)((uint16_t)inMsg[2] << (uint16_t)0) | (uint16_t)((uint16_t)inMsg[3] << (uint16_t)8)), // year
		(uint8_t)inMsg[4], // month
		(uint8_t)inMsg[5], // day
		(uint8_t)inMsg[6], // hour
		(uint8_t)inMsg[7], // minute
		(uint16_t)((uint16_t)((uint16_t)inMsg[8] << (uint16_t)0) | (uint16_t)((uint16_t)inMsg[9] << (uint16_t)8)) // miliseconds
	};
	outMsg->type = static_cast<NetworkMessageType>(((uint16_t)inMsg[10] << 0) | ((uint16_t)inMsg[11] << 8));
	outMsg->data = new char[outMsg->dataSize];
	memcpy(outMsg->data, &inMsg[12], outMsg->dataSize);
}

char* GenerateNetworkMessage(const NetworkMessage* inMsg, int* generatedMessageSize)
{
	int bufferSize = sizeof(TimeStamp) + sizeof(NetworkMessageType) + sizeof(uint16_t) + inMsg->dataSize;
	char* ret = new char[bufferSize];
	
	ret[0] = inMsg->dataSize & 0xFF;
	ret[1] = inMsg->dataSize & 0xFF00;
	
	ret[2] = inMsg->time.year & 0xFF;
	ret[3] = inMsg->time.year & 0xFF00;
	ret[4] = inMsg->time.month;
	ret[5] = inMsg->time.day;
	ret[6] = inMsg->time.hour;
	ret[7] = inMsg->time.minute;
	ret[8] = inMsg->time.miliseconds & 0xFF;
	ret[9] = inMsg->time.miliseconds & 0xFF00;
	
	ret[10] = ((int)inMsg->type & 0xFF);
	ret[11] = ((int)inMsg->type & 0xFF00);
	memcpy(&ret[12], inMsg->data, inMsg->dataSize);

	*generatedMessageSize = 12 + inMsg->dataSize;

	return ret;
}