#include "Clock.h"

#include <time.h>

void Clock::GetCurrentTimeStamp(TimeStamp* stamp)
{
    time_t t = time(nullptr);

    tm* now = localtime(&t);
    stamp->day = now->tm_mday;
    stamp->hour = now->tm_hour;
    stamp->miliseconds = 0;
    stamp->month = now->tm_mon + 1;
    stamp->year = now->tm_year + 1900;
}

TimeStamp Clock::GetCurrentTimeStamp()
{
    TimeStamp ret;
    Clock::GetCurrentTimeStamp(&ret);
    return ret;
}