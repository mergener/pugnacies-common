#pragma once

#include <stdint.h>

#include "Clock.h"

enum class NetworkMessageType : uint16_t
{
	//
	//	Client to server:
	//

	NETMSG_CTS_LOGINREQ,

	//
	//	Server to client:
	//
	
	NETMSG_STC_LOGINACCEPTED
};

struct NetworkMessage
{
	TimeStamp time;
	char* data;
	NetworkMessageType type;
	uint16_t dataSize;
};

void TranslateNetworkMessage(const char* const inMsg, NetworkMessage* outMsg);
char* GenerateNetworkMessage(const NetworkMessage* inMsg, int* generatedMessageSize);