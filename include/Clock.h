#pragma once

#include <stdint.h>

struct TimeStamp
{
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint16_t miliseconds;

	inline uint16_t seconds() const { return miliseconds / 1000; }
};

namespace Clock
{
    TimeStamp GetCurrentTimeStamp();
    void GetCurrentTimeStamp(TimeStamp* timeStamp);
}